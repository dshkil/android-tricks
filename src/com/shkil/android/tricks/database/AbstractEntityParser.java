package com.shkil.android.tricks.database;

import com.shkil.android.tricks.database.DbUtils.Entity;
import com.shkil.android.tricks.database.DbUtils.EntityParser;

import android.database.Cursor;

public abstract class AbstractEntityParser<T extends Entity<? super T>> implements EntityParser<T> {

	private final EntityParser<? super T> superParser;

	public AbstractEntityParser() {
		this(null);
	}

	public AbstractEntityParser(EntityParser<? super T> superParser) {
		this.superParser = superParser;
	}

	@Override
	public abstract T newEntity();

	@Override
	public final void parseCursor(T item, Cursor c) {
		superParser.parseCursor(item, c);
		parseCursor(c, item);
	}

	protected abstract void parseCursor(Cursor c, T item);

}
