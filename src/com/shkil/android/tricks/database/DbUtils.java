package com.shkil.android.tricks.database;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;

public class DbUtils {

	public static boolean LOG;
	public static final String TAG = DbUtils.class.getSimpleName();

	public static interface Column {
		int index();
		String column();
	}

	public static interface ColumnEx extends Column {
		String getColumnDefinitions();
	}

	public interface Entity<T> {
		String getTable();
		ContentValues toContentValues();
		String getPrimaryWhereClause();
		String[] getPrimaryWhereArgs();
	}

	public interface EntityParser<T extends Entity<? super T>> {
		T newEntity();
		void parseCursor(T entity, Cursor c);
	}

	public interface QueryHelper<T extends Entity<T>> {
		String buildQueryString(String orderBy, String limit);
		T parseCursor(Cursor c);
	}

	public static abstract class TinyQueryHelper<T extends Entity<T>> implements QueryHelper<T> {
		private final EntityParser<T> entityParser;
		private String query;

		public TinyQueryHelper(EntityParser<T> entityParser) {
			this.entityParser = entityParser;
		}
		@Override
		public final String buildQueryString(String orderBy, String limit) {
			if (orderBy != null || limit != null) {
				throw new IllegalArgumentException("orderBy and limit is not supported");
			}
			if (query == null) {
				query = buildQueryString();
			}
			return query;
		}
		public abstract String buildQueryString();
		@Override
		public T parseCursor(Cursor c) {
			T entity = entityParser.newEntity();
			entityParser.parseCursor(entity, c);
			return entity;
		}
	}

	public static <T extends Entity<T>> QueryHelper<T> newQueryHelper(String table, String where, final EntityParser<T> parser, Column... columns) {
		return newQueryHelper(table, where, parser, getColumnNames(columns));
	}

	public static <T extends Entity<T>> QueryHelper<T> newQueryHelper(String table, String where, final EntityParser<T> parser, String... columns) {
		return new SimpleQueryHelper<T>(table, where, columns) {
			@Override
			public T parseCursor(Cursor c) {
				T entity = parser.newEntity();
				parser.parseCursor(entity, c);
				return entity;
			}
		};
	}

	public static abstract class SimpleQueryHelper<T extends Entity<T>> implements QueryHelper<T> {
		private String table;
		private String[] columns;
		private String whereClause;
		private String simpleSql;

		public SimpleQueryHelper(String table, String whereClause, Column... columns) {
			this(table, whereClause, getColumnNames(columns));
		}
		public SimpleQueryHelper(String table, String where, String... columns) {
			this.table = table;
			this.columns = columns;
			this.whereClause = where;
			this.simpleSql = SQLiteQueryBuilder.buildQueryString(false, table, columns, where, null, null, null, null);
		}
		@Override
		public String buildQueryString(String orderBy, String limit) {
			if (orderBy == null && limit == null) {
				return simpleSql;
			}
			return SQLiteQueryBuilder.buildQueryString(false, table, columns, whereClause, null, null, orderBy, limit);
		}
		@Override
		public abstract T parseCursor(Cursor c);
	}

	public static void enableDebugLogging(boolean enabled) {
		LOG = enabled;
	}

	public static String[] getColumnNames(Column... columns) {
		int count = columns.length;
		String[] result = new String[count];
		for (int i = 0; i < count; i++) {
			result[i] = columns[i].column();
		}
		return result;
	}

	public static String[] getColumnNames(String prefix, Column... columns) {
		int count = columns.length;
		String[] result = new String[count];
		for (int i = 0; i < count; i++) {
			result[i] = prefix + columns[i].column();
		}
		return result;
	}

	public static Cursor query(SQLiteDatabase db, String table, Column[] columns, String whereClause, String[] whereArgs, String orderBy, String limit) {
		return db.query(table, getColumnNames(columns), whereClause, whereArgs, null, null, orderBy, limit);
	}

	public static <T extends Entity<T>> Cursor query(SQLiteDatabase db, QueryHelper<T> helper, String[] whereArgs, String orderBy, String limit) {
		String sql = helper.buildQueryString(orderBy, limit);
		return db.rawQuery(sql, whereArgs);
	}

	public static <T extends Entity<T>> List<T> queryEntities(SQLiteDatabase db, QueryHelper<T> helper, String[] whereArgs) {
		return queryEntities(db, helper, whereArgs, (String) null, null);
	}

	public static <T extends Entity<T>> List<T> queryEntities(SQLiteDatabase db, QueryHelper<T> helper, String[] whereArgs, Column orderBy, String limit) {
		return queryEntities(db, helper, whereArgs, orderBy.column(), limit);
	}

	public static <T extends Entity<T>> List<T> queryEntities(SQLiteDatabase db, QueryHelper<T> helper, String[] whereArgs, String orderBy, String limit) {
		String sql = helper.buildQueryString(orderBy, limit);
		Cursor cursor = db.rawQuery(sql, whereArgs);
		if (cursor != null) {
			try {
				if (cursor.moveToFirst()) {
					List<T> result = new ArrayList<T>(cursor.getCount());
					do {
						result.add(helper.parseCursor(cursor));
					}
					while (cursor.moveToNext());
					return result;
				}
			}
			finally {
				cursor.close();
			}
		}
		return Collections.emptyList();
	}

	public static <T extends Entity<T>> T queryEntity(SQLiteDatabase db, QueryHelper<T> helper, String[] whereArgs) {
		String sql = helper.buildQueryString(null, null);
		Cursor cursor = db.rawQuery(sql, whereArgs);
		if (cursor != null) {
			try {
				if (cursor.moveToFirst()) {
					return helper.parseCursor(cursor);
				}
			}
			finally {
				cursor.close();
			}
		}
		return null;
	}

	/**
	 * @return the row ID of the newly inserted row, 0 if the entity was updated, or -1 if an error occurred
	 */
	public static <T extends Entity<T>> long saveEntity(SQLiteDatabase db, Entity<T> entity) throws SQLException {
		String table = entity.getTable();
		ContentValues values = entity.toContentValues();
		String whereClause = entity.getPrimaryWhereClause();
		String[] whereArgs = entity.getPrimaryWhereArgs();
		if (whereClause != null && db.update(table, values, whereClause, whereArgs) > 0) {
			if (LOG) {
				Log.d(TAG, "Entity " + entity.getClass().getSimpleName() + " updated: whereArgs=" + Arrays.toString(whereArgs) + ", values=" + values);
			}
			return 0;
		}
		if (LOG) {
			Log.d(TAG, "Replacing " + entity.getClass().getSimpleName() + " entity: " + values);
		}
		return db.replaceOrThrow(table, null, values);
	}

	/**
	 * @return the row ID, or -1 if an error occurred
	 */
	public static <T extends Entity<T>> long saveEntityEx(SQLiteDatabase db, Entity<T> entity, Column idColumn) throws SQLException {
		return saveEntityEx(db, entity, idColumn.column());
	}

	/**
	 * @return the row ID, or -1 if an error occurred
	 */
	public static <T extends Entity<T>> long saveEntityEx(SQLiteDatabase db, Entity<T> entity, String idColumn) throws SQLException {
		String table = entity.getTable();
		ContentValues values = entity.toContentValues();
		String whereClause = entity.getPrimaryWhereClause();
		String[] whereArgs = entity.getPrimaryWhereArgs();
		if (whereClause != null && db.update(table, values, whereClause, whereArgs) > 0) {
			if (LOG) {
				Log.d(TAG, "Entity " + entity.getClass().getSimpleName() + " updated: whereArgs=" + Arrays.toString(whereArgs) + ", values=" + values);
			}
			Long id = values.getAsLong(idColumn);
			if (id == null || id <= 0) {
				Cursor c = db.query(table, new String[] { idColumn }, whereClause, whereArgs, null, null, null);
				try {
					return c.moveToFirst() ? c.getLong(0) : 0;
				}
				finally {
					c.close();
				}
			}
			return id;
		}
		if (LOG) {
			Log.d(TAG, "Replacing " + entity.getClass().getSimpleName() + " entity: " + values);
		}
		return db.replaceOrThrow(table, null, values);
	}

	public static <T extends ColumnEx> void createTable(SQLiteDatabase db, String tableName, T[] columns, T[] compositeUnique) {
		db.execSQL(buildCreateTableSql(tableName, Arrays.asList(columns), compositeUnique));
	}

	public static <T extends ColumnEx> void createTable(SQLiteDatabase db, String tableName, List<T> columns, T[] compositeUnique) {
		db.execSQL(buildCreateTableSql(tableName, columns, compositeUnique));
	}

	public static void dropTable(SQLiteDatabase db, String tableName) {
		db.execSQL("DROP TABLE IF EXISTS " + tableName);
	}

	public static <T extends Enum<T> & ColumnEx> void addColumn(SQLiteDatabase db, String tableName, T column) {
		db.execSQL(buildAddColumnSql(tableName, column));
	}

	public static <T extends Enum<T> & ColumnEx> void addColumns(SQLiteDatabase db, String tableName, T... columns) {
		for (T column : columns) {
			addColumn(db, tableName, column);
		}
	}

	private static <T extends ColumnEx> String buildCreateTableSql(String tableName, List<T> columns, T[] compositeUnique) {
		StringBuilder sb = new StringBuilder(300)
			.append("CREATE TABLE IF NOT EXISTS ").append(tableName).append(" (");
		for (int i = 0; i < columns.size(); i++) {
			if (i > 0) {
				sb.append(", ");
			}
			ColumnEx column = columns.get(i);
			sb.append(column.column()).append(" ").append(column.getColumnDefinitions());
		}
		if (compositeUnique != null && compositeUnique.length > 0) {
			sb.append(", UNIQUE(");
			for (int i = 0; i < compositeUnique.length; i++) {
				if (i > 0) {
					sb.append(',');
				}
				sb.append(compositeUnique[i].column());
			}
			sb.append(")");
		}
		return sb.append(")").toString();
	}

	private static <T extends Enum<T> & ColumnEx> String buildAddColumnSql(String tableName, T column) {
		StringBuilder sb = new StringBuilder(300)
			.append("ALTER TABLE ").append(tableName).append(" ADD COLUMN ").append(column.column()).append(" ").append(column.getColumnDefinitions());
		return sb.toString();
	}

	public static String innerJoin(String table1, Column onColumn1, String table2, Column onColumn2) {
		return " " + table1 + " t1 INNER JOIN " + table2 + " t2 ON (t1." + onColumn1.column() + "=t2." + onColumn2.column() + ") ";
	}

	public static String where(Column... columns) {
		return where(null, columns);
	}

	public static String where(String prefix, Column... columns) {
		StringBuilder sb = new StringBuilder();
		for (Column column : columns) {
			if (sb.length() > 0) {
				sb.append("AND ");
			}
			else {
				sb.append(" WHERE ");
			}
			if (prefix != null) {
				sb.append(prefix);
			}
			sb.append(column.column()).append("=? ");
		}
		return sb.toString();
	}

}
