package com.shkil.android.tricks.database;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.util.SparseArray;

import com.shkil.android.tricks.database.DbUtils.ColumnEx;

public class ColumnsBuilder {

	private static class ColumnImpl implements ColumnEx {
		private final int index;
		private final String columnName;
		private final String columnDefinitions;

		protected ColumnImpl(int index, String name, String definitions) {
			this.index = index;
			this.columnName = name;
			this.columnDefinitions = definitions;
		}
		@Override
		public int index() {
			return index;
		}
		@Override
		public String column() {
			return columnName;
		}
		@Override
		public String getColumnDefinitions() {
			return columnDefinitions;
		}
		@Override
		public String toString() {
			return columnName;
		}
	}

	private static final SparseArray<ColumnsBuilder> builders = new SparseArray<ColumnsBuilder>(24);

	public static ColumnsBuilder of(Class<?> clazz) {
		int classHashCode = clazz.hashCode();
		ColumnsBuilder columnBuilder = builders.get(classHashCode);
		if (columnBuilder != null) {
			return columnBuilder;
		}
		Class<?> superClass = clazz.getSuperclass();
		if (superClass != Object.class) {
			columnBuilder = new ColumnsBuilder(of(superClass));
		}
		else {
			columnBuilder = new ColumnsBuilder();
		}
		builders.put(classHashCode, columnBuilder);
		return columnBuilder;
	}

	private int columnCount;
	private List<ColumnEx> columns = new ArrayList<ColumnEx>();

	private ColumnsBuilder() {}

	private ColumnsBuilder(ColumnsBuilder superColumnBuilder) {
		columns.addAll(superColumnBuilder.columns);
		columnCount += superColumnBuilder.columnCount;
	}

	public ColumnEx create(String name, String title) {
		ColumnImpl column = new ColumnImpl(columnCount++, name, title);
		columns.add(column);
		return column;
	}

	public int getColumnCount() {
		return columnCount;
	}

	public List<ColumnEx> commit() {
		return Collections.unmodifiableList(columns);
	}

}
