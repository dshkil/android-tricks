package com.shkil.android.tricks;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import android.app.Application;
import android.graphics.Bitmap;
import android.util.Log;

import com.shkil.android.util.concurrent.Fetcher;
import com.shkil.android.util.concurrent.Fetcher.FetchingResult;
import com.shkil.android.util.concurrent.Fetcher.FutureListener;
import com.shkil.android.util.concurrent.FetcherWithCache;
import com.shkil.android.util.concurrent.MainThreadExecutor;
import com.shkil.android.util.concurrent.Priority;

public class DemoApp extends Application {

	private static final String TAG = DemoApp.class.getSimpleName();

	private static final ThreadFactory THREAD_FACTORY = new ThreadFactory() {
		private final AtomicInteger count = new AtomicInteger();

		@Override
		public Thread newThread(Runnable r) {
			return new Thread(r, "[thread-" + count.incrementAndGet() + "]");
		}
	};

	private static final ExecutorService IMAGE_LOADER_EXECUTOR = Executors.newFixedThreadPool(1, THREAD_FACTORY);
	private static final ExecutorService THREAD_POOL_EXECUTOR = new ThreadPoolExecutor(4, 6, 5L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(), THREAD_FACTORY);

	private static final Executor MAIN_EXECUTOR = MainThreadExecutor.getInstance();

	public static final int MAX_IMAGE_CACHE_SIZE = (int) (Runtime.getRuntime().maxMemory() / 6);

	private final Fetcher<String,Bitmap> imageFetcher = new FetcherWithCache<String,Bitmap>(IMAGE_LOADER_EXECUTOR, MAIN_EXECUTOR, MAX_IMAGE_CACHE_SIZE) {
		@Override
		protected Bitmap fetchValue(String key) throws Exception {
			return null; //TODO implement downloading from network
		}
	};

	@Override
	public void onCreate() {
		super.onCreate();
		imageFetcher.fetch("http://example.com/image.png", Priority.BACKGROUND).setListener(
			new FutureListener<Bitmap>() { // prefetch cool image
				@Override
				public void onFutureResult(FetchingResult<Bitmap> result) {
					Log.d(TAG, "Image received (async)");
				}
			});
		executeAsync(new Runnable() {
			@Override
			public void run() {
				try {
					FetchingResult<Bitmap> result = imageFetcher.fetch("http://example.com/image.png").get(); // .get() method will wait for result
					Bitmap image = result.getValueOrThrow();
					if (image != null) {
						Log.d(TAG, "Image downloaded");
					}
					else {
						Log.d(TAG, "Image not found");
					}
				}
				catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});
	}

	public void executeAsync(Runnable command) {
		THREAD_POOL_EXECUTOR.execute(command);
	}

}
