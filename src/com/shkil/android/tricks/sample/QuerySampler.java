package com.shkil.android.tricks.sample;

import java.util.List;

import android.database.sqlite.SQLiteDatabase;

import com.shkil.android.tricks.database.DbUtils;
import com.shkil.android.tricks.sample.model.SampleData;
import com.shkil.android.tricks.sample.model.SampleData.ByParentQueryHelper;
import com.shkil.android.tricks.sample.model.SampleData.SampleDataQueryHelper;

public class QuerySampler {

	private final SQLiteDatabase db;

	public QuerySampler(DbHelper dbHelper) {
		this.db = dbHelper.getWritableDatabase();
	}

	public long save(SampleData data) {
		return DbUtils.saveEntity(db, data);
	}

	public SampleData querySampleData(String id, String userId) {
		return DbUtils.queryEntity(db, SampleData.getQuery(), SampleDataQueryHelper.makeWhereArgs(id, userId));
	}

	public List<SampleData> querySampleDataByParent(String parentId, String userId) {
		return DbUtils.queryEntities(db, SampleData.getByParentQuery(), ByParentQueryHelper.makeWhereArgs(parentId, userId));
	}

}
