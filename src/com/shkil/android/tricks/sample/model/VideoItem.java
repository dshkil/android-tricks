package com.shkil.android.tricks.sample.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.shkil.android.tricks.database.AbstractEntityParser;
import com.shkil.android.tricks.database.DbUtils.EntityParser;
import com.shkil.android.tricks.sample.db.VideoItemColumn;

public class VideoItem extends MediaItem {

	private int bitrate;

	public int getBitrate() {
		return bitrate;
	}

	public void setBitrate(int bitrate) {
		this.bitrate = bitrate;
	}

	/*
	 * database related stuff
	 */

	@Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		values.put(VideoItemColumn.BITRATE.column(), bitrate);
		return values;
	}

	protected static class VideoItemEntityParser extends AbstractEntityParser<VideoItem> {
		protected static final VideoItemEntityParser INSTANCE = new VideoItemEntityParser();

		public VideoItemEntityParser() {
			super(getMediaItemParser());
		}
		@Override
		public VideoItem newEntity() {
			return new VideoItem();
		}
		@Override
		public void parseCursor(Cursor c, VideoItem item) {
			super.parseCursor(item, c);
		}
	}

	public static EntityParser<VideoItem> getVideoItemParser() {
		return VideoItemEntityParser.INSTANCE;
	}

}
