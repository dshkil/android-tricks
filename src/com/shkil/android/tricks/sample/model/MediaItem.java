package com.shkil.android.tricks.sample.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.shkil.android.tricks.database.AbstractEntityParser;
import com.shkil.android.tricks.database.DbUtils.Entity;
import com.shkil.android.tricks.database.DbUtils.EntityParser;
import com.shkil.android.tricks.sample.DbHelper;
import com.shkil.android.tricks.sample.db.MediaItemColumn;

public class MediaItem implements Entity<MediaItem> {

	private long id;
	private String title;
	private String url;

	public static final String PRIMARY_WHERE = MediaItemColumn.ID + "=?";

	public long getId() {
		return id;
	}

	protected void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	/*
	 * database related stuff
	 */

	@Override
	public String getTable() {
		return DbHelper.TABLE_MEDIA_ITEM;
	}

	@Override
	public String getPrimaryWhereClause() {
		return PRIMARY_WHERE;
	}

	@Override
	public String[] getPrimaryWhereArgs() {
		return new String[] { String.valueOf(id) };
	}

	@Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		values.put(MediaItemColumn.ID.column(), id);
		values.put(MediaItemColumn.TITLE.column(), title);
		values.put(MediaItemColumn.URL.column(), url);
		return values;
	}

	protected static class MediaItemParser extends AbstractEntityParser<MediaItem> {
		protected static final MediaItemParser INSTANCE = new MediaItemParser();

		@Override
		public MediaItem newEntity() {
			return new MediaItem();
		}
		@Override
		public void parseCursor(Cursor c, MediaItem item) {
			item.setId(c.getLong(MediaItemColumn.ID.index()));
			item.setTitle(c.getString(MediaItemColumn.TITLE.index()));
			item.setUrl(c.getString(MediaItemColumn.URL.index()));
		}
	}

	public static EntityParser<MediaItem> getMediaItemParser() {
		return MediaItemParser.INSTANCE;
	}

}
