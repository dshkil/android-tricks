package com.shkil.android.tricks.sample.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteQueryBuilder;

import com.shkil.android.tricks.database.DbUtils;
import com.shkil.android.tricks.database.DbUtils.Entity;
import com.shkil.android.tricks.database.DbUtils.QueryHelper;
import com.shkil.android.tricks.sample.DbHelper;
import com.shkil.android.tricks.sample.db.EnumSampleDataColumn;

public class SampleData implements Entity<SampleData> {

	private final String userId;
	private final String channelId;
	private String parentId;
	private long syncTime;
	private String description;
	private String data;

	static final String PRIMARY_WHERE = EnumSampleDataColumn.CHANNEL_ID + "=? AND " + EnumSampleDataColumn.USER_ID + "=?";

	public SampleData(String userId, String channelId) {
		this.userId = userId;
		this.channelId = channelId;
	}

	public String getUserId() {
		return userId;
	}

	public String getChannelId() {
		return channelId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public long getSyncTime() {
		return syncTime;
	}

	public void setSyncTime(long syncTime) {
		this.syncTime = syncTime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	/*
	 * database related stuff
	 */

	@Override
	public String getTable() {
		return DbHelper.TABLE_SAMPLE_DATA;
	}

	@Override
	public String getPrimaryWhereClause() {
		return PRIMARY_WHERE;
	}

	@Override
	public String[] getPrimaryWhereArgs() {
		return new String[] { channelId, userId };
	}

	@Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		values.put(EnumSampleDataColumn.CHANNEL_ID.column(), channelId);
		values.put(EnumSampleDataColumn.PARENT_ID.column(), parentId);
		values.put(EnumSampleDataColumn.USER_ID.column(), userId);
		values.put(EnumSampleDataColumn.DATA.column(), data);
		values.put(EnumSampleDataColumn.DESCRIPTION.column(), description);
		if (syncTime > 0) {
			values.put(EnumSampleDataColumn.SYNC_TIME.column(), syncTime);
		}
		return values;
	}

	private abstract static class AbstractSampleDataQueryHelper implements QueryHelper<SampleData> {
		@Override
		public SampleData parseCursor(Cursor c) {
			String channelId = c.getString(EnumSampleDataColumn.CHANNEL_ID.index());
			String userId = c.getString(EnumSampleDataColumn.USER_ID.index());
			SampleData result = new SampleData(userId, channelId);
			result.setParentId(c.getString(EnumSampleDataColumn.PARENT_ID.index()));
			result.setSyncTime(c.getLong(EnumSampleDataColumn.SYNC_TIME.index()));
			result.setDescription(c.getString(EnumSampleDataColumn.DESCRIPTION.index()));
			result.setData(c.getString(EnumSampleDataColumn.DATA.index()));
			return result;
		}
	}

	public static class SampleDataQueryHelper extends AbstractSampleDataQueryHelper {
		protected static final SampleDataQueryHelper INSTANCE = new SampleDataQueryHelper();
		private static final String[] COLUMNS = DbUtils.getColumnNames(EnumSampleDataColumn.values()); //all columns
		private static final String SIMPLE_SQL = SQLiteQueryBuilder.buildQueryString(false, DbHelper.TABLE_SAMPLE_DATA, COLUMNS, PRIMARY_WHERE, null, null, null, null);
		@Override
		public String buildQueryString(String orderBy, String limit) {
			if (orderBy == null && limit == null) {
				return SIMPLE_SQL;
			}
			return SQLiteQueryBuilder.buildQueryString(false, DbHelper.TABLE_SAMPLE_DATA, COLUMNS, PRIMARY_WHERE, null, null, orderBy, limit);
		}
		public static String[] makeWhereArgs(String channelId, String userId) {
			return new String[] { channelId, userId };
		}
	}

	public static class ByParentQueryHelper extends AbstractSampleDataQueryHelper {
		protected static final ByParentQueryHelper INSTANCE = new ByParentQueryHelper();
		private static final String[] COLUMNS = DbUtils.getColumnNames(EnumSampleDataColumn.values()); //all columns
		private static final String WHERE = EnumSampleDataColumn.PARENT_ID + "=? AND " + EnumSampleDataColumn.USER_ID + "=?";
		private static final String SIMPLE_SQL = SQLiteQueryBuilder.buildQueryString(false, DbHelper.TABLE_SAMPLE_DATA, COLUMNS, WHERE, null, null, null, null);
		@Override
		public String buildQueryString(String orderBy, String limit) {
			if (orderBy == null && limit == null) {
				return SIMPLE_SQL;
			}
			return SQLiteQueryBuilder.buildQueryString(false, DbHelper.TABLE_SAMPLE_DATA, COLUMNS, WHERE, null, null, orderBy, limit);
		}
		public static String[] makeWhereArgs(String parentId, String userId) {
			return new String[] { parentId, userId };
		}
	}

	public static SampleDataQueryHelper getQuery() {
		return SampleDataQueryHelper.INSTANCE;
	}

	public static ByParentQueryHelper getByParentQuery() {
		return ByParentQueryHelper.INSTANCE;
	}

}
