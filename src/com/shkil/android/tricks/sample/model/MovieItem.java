package com.shkil.android.tricks.sample.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.shkil.android.tricks.database.AbstractEntityParser;
import com.shkil.android.tricks.sample.db.MovieItemColumn;

public class MovieItem extends VideoItem {
	
	private String director;
	
	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}
	
	@Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		values.put(MovieItemColumn.DIRECTOR.column(), director);
		return values;
	}

	protected static class MovieItemEntityParser extends AbstractEntityParser<MovieItem> {
		public MovieItemEntityParser() {
			super(getVideoItemParser());
		}
		@Override
		public MovieItem newEntity() {
			return new MovieItem();
		}
		@Override
		public void parseCursor(Cursor c, MovieItem item) {}
	}

}
