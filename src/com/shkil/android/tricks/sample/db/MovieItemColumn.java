package com.shkil.android.tricks.sample.db;

import java.util.List;

import com.shkil.android.tricks.database.ColumnsBuilder;
import com.shkil.android.tricks.database.DbUtils.ColumnEx;

public class MovieItemColumn extends MediaItemColumn {

	private static final ColumnsBuilder b = ColumnsBuilder.of(MovieItemColumn.class);

	public static final ColumnEx DIRECTOR = b.create("director", "text");

	public static final List<ColumnEx> COLUMNS = b.commit();

}
