package com.shkil.android.tricks.sample.db;

import com.shkil.android.tricks.database.DbUtils.ColumnEx;

import android.provider.BaseColumns;

public enum EnumSampleDataColumn implements ColumnEx {

	ID(BaseColumns._ID, "integer primary key autoincrement"),
	CHANNEL_ID("channelId", "text not null"),
	PARENT_ID("parentId", "text not null"),
	USER_ID("userId", "text not null"),
	SYNC_TIME("syncTime", "int not null default 0"),
	DESCRIPTION("description", "text"),
	DATA("data", "text");

	public static final EnumSampleDataColumn[] COMPOSITE_UNIQUE_CONSTRAINT = { CHANNEL_ID, USER_ID };

	private final String columnName;
	private final String columnDefinitions;

	private EnumSampleDataColumn(String columnName, String columnDefinitions) {
		this.columnName = columnName;
		this.columnDefinitions = columnDefinitions;
	}

	@Override
	public int index() {
		return ordinal();
	}

	@Override
	public String column() {
		return columnName;
	}

	@Override
	public String toString() {
		return columnName;
	}

	@Override
	public String getColumnDefinitions() {
		return columnDefinitions;
	}

}
