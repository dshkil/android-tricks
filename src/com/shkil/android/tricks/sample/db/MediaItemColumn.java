package com.shkil.android.tricks.sample.db;

import java.util.List;

import com.shkil.android.tricks.database.ColumnsBuilder;
import com.shkil.android.tricks.database.DbUtils.ColumnEx;

public class MediaItemColumn {

	private static final ColumnsBuilder b = ColumnsBuilder.of(MediaItemColumn.class);

	public static final ColumnEx ID = b.create("id", "number not null");
	public static final ColumnEx TITLE = b.create("title", "text not null");
	public static final ColumnEx URL = b.create("url", "text not null");

	public static final List<ColumnEx> COLUMNS = b.commit();

}
