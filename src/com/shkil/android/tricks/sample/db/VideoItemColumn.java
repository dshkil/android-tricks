package com.shkil.android.tricks.sample.db;

import java.util.List;

import com.shkil.android.tricks.database.ColumnsBuilder;
import com.shkil.android.tricks.database.DbUtils.ColumnEx;

public class VideoItemColumn extends MediaItemColumn {

	private static final ColumnsBuilder b = ColumnsBuilder.of(VideoItemColumn.class);

	public static final ColumnEx BITRATE = b.create("bitrate", "number not null default 0");

	public static final List<ColumnEx> COLUMNS = b.commit();

}
