package com.shkil.android.tricks.sample;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.shkil.android.tricks.database.DbUtils;
import com.shkil.android.tricks.sample.db.EnumSampleDataColumn;
import com.shkil.android.tricks.sample.db.MovieItemColumn;

public class DbHelper extends SQLiteOpenHelper {

	public static final String DB_NAME = "tricks.db";
	public static final int DB_VERSION = 4;

	public static final String TABLE_SAMPLE_DATA = "sample_data";
	public static final String TABLE_MEDIA_ITEM = "media_items";

	public DbHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		DbUtils.createTable(db, TABLE_SAMPLE_DATA, EnumSampleDataColumn.values(), EnumSampleDataColumn.COMPOSITE_UNIQUE_CONSTRAINT);
		DbUtils.createTable(db, TABLE_MEDIA_ITEM, MovieItemColumn.COLUMNS, null); //FIXME implement shared table creation for several models
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if (oldVersion < 2) {
			DbUtils.addColumn(db, TABLE_SAMPLE_DATA, EnumSampleDataColumn.PARENT_ID);
		}
		if (oldVersion < 3) {
			DbUtils.addColumns(db, TABLE_SAMPLE_DATA, EnumSampleDataColumn.DESCRIPTION, EnumSampleDataColumn.DATA);
		}
		if (oldVersion < 4) {
			DbUtils.createTable(db, TABLE_MEDIA_ITEM, MovieItemColumn.COLUMNS, null); //FIXME implement shared table creation for several models
		}
	}

}
