package com.shkil.android.tricks.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

/**
 * Usage sample:
 * 		startActivity(ArgumentedActivity.createIntent(this, channelId, sampleValue));
 * To make code clear, direct intent instantiation is not recommended
 * @author Dmytro Shkil
 */
public class ArgumentedActivity extends Activity {

	private static final String TAG = ArgumentedActivity.class.getSimpleName();

	private static final String EXTRA_CHANNEL_ID = "channel-id";
	private static final String EXTRA_SAMPLE_VALUE = "sample-value";

	public static Intent createIntent(Context context, String channelId) {
		return new Intent(context, ArgumentedActivity.class)
		.putExtra(EXTRA_CHANNEL_ID, channelId);
	}

	public static Intent createIntent(Context context, String channelId, int sampleValue) {
		return new Intent(context, ArgumentedActivity.class)
		.putExtra(EXTRA_CHANNEL_ID, channelId)
		.putExtra(EXTRA_SAMPLE_VALUE, sampleValue);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle extras = getIntent().getExtras();
		String channelId = extras.getString(EXTRA_CHANNEL_ID);
		int sampleValue = extras.getInt(EXTRA_SAMPLE_VALUE, 0);
		Log.d(TAG, "onCreate(): channelId=" + channelId + ", sampleValue=" + sampleValue);
	}

}
