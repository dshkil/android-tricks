package com.shkil.android.util;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.WillClose;

import android.annotation.TargetApi;
import android.database.DatabaseUtils;
import android.net.TrafficStats;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;

import com.shkil.android.tricks.BuildConfig;
import com.shkil.android.util.io.DirectByteArrayOutputStream;
import com.shkil.android.util.io.DirectByteBuffer;

public class Utils {

	public static String getTag(Class<?> clazz) {
		return clazz.getSimpleName();
	}

	public static void preventRunningOnMainThread() {
		if (BuildConfig.DEBUG && Thread.currentThread() == Looper.getMainLooper().getThread()) {
			throw new RuntimeException("Possibly long operation has been running on the main thread");
		}
	}

	public static void ensureRunningOnMainThread() {
		if (BuildConfig.DEBUG && Thread.currentThread() != Looper.getMainLooper().getThread()) {
			throw new RuntimeException("Should be running on the main thread");
		}
	}

	public static boolean isRunningOnMainThread() {
		return Thread.currentThread() == Looper.getMainLooper().getThread();
	}

	/**
	 * @param name name of thread, use {0} for thread counter
	 */
	public static ThreadFactory newThreadFactory(final String name) {
		return new ThreadFactory() {
			private final AtomicInteger counter = new AtomicInteger();

			@Override
			public Thread newThread(Runnable r) {
				return new Thread(r, MessageFormat.format(name, counter.incrementAndGet()));
			}
		};
	}

	@WillClose
	public static DirectByteBuffer readFully(InputStream inputStream) throws IOException {
		try {
			DirectByteArrayOutputStream outputStream = new DirectByteArrayOutputStream();
			final byte[] buffer = new byte[8192];
			int len;
			while ((len = inputStream.read(buffer)) != -1) {
				outputStream.write(buffer, 0, len);
			}
			return outputStream;
		}
		finally {
			inputStream.close();
		}
	}

	public static String calculateMd5Hash(String value) {
		if (value == null || value.length() == 0) {
			return null;
		}
		StringBuilder buffer = new StringBuilder();
		calculateMd5Hash(value, buffer);
		return buffer.toString();
	}

	public static void calculateMd5Hash(String value, StringBuilder output) {
		if (value == null || value.length() == 0) {
			return;
		}
		try {
			MessageDigest hashEngine = MessageDigest.getInstance("MD5");
			hashEngine.update(value.getBytes());
			toHex(hashEngine.digest(), output);
		}
		catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	public static String toHex(byte[] data) {
		if (data == null || data.length == 0) {
			return "";
		}
		StringBuilder buffer = new StringBuilder(data.length * 2);
		toHex(data, buffer);
		return buffer.toString();
	}

	public static void toHex(byte[] data, StringBuilder output) {
		if (data == null || data.length == 0) {
			return;
		}
		for (int i = 0; i < data.length; i++) {
			byte dataByte = data[i];
			int halfbyte = (dataByte >>> 4) & 0x0F;
			int two_halfs = 0;
			do {
				if (halfbyte >= 0 && halfbyte <= 9) {
					output.append((char) ('0' + halfbyte));
				}
				else {
					output.append((char) ('a' + halfbyte - 10));
				}
				halfbyte = dataByte & 0x0F;
			}
			while (two_halfs++ < 1);
		}
	}

	public static boolean isNotEmpty(CharSequence string) {
		return string != null && string.length() > 0;
	}

	public static boolean isEmpty(CharSequence value) {
		return value == null || value.length() == 0;
	}

	public static boolean isNotEmpty(List<?> list) {
		return list != null && list.size() > 0;
	}

	public static boolean isNotEmpty(Object[] array) {
		return array != null && array.length > 0;
	}

	/**
	 * Helper function to write a String during serialization. We need this because java.io.writeUTF does not accept a null. Used in conjuction with Utils.readString
	 */
	public static void writeString(DataOutput output, String value) throws IOException {
		if (value != null) {
			output.writeBoolean(true);
			output.writeUTF(value);
		}
		else {
			output.writeBoolean(false);
		}
	}

	public static String readString(DataInput input) throws IOException {
		if (input.readBoolean()) {
			return input.readUTF();
		}
		return null;
	}

	@TargetApi(14)
	public static void setThreadTrafficStatsTag(int tag) {
		if (Build.VERSION.SDK_INT >= 14) {
			TrafficStats.setThreadStatsTag(tag);
		}
	}

	@TargetApi(14)
	public static void clearThreadTrafficStatsTag() {
		if (Build.VERSION.SDK_INT >= 14) {
			TrafficStats.clearThreadStatsTag();
		}
	}

	public static String toString(Bundle bundle) {
		if (bundle != null) {
			bundle.size(); // force unparcell
		}
		return String.valueOf(bundle);
	}

	@TargetApi(9)
	public static IOException newIOException(Exception cause) {
		return Build.VERSION.SDK_INT >= 9 ? new IOException(cause) : new IOException(cause.toString());
	}

	/**
	 * Join list elements with a glue string.
	 */
	public static String join(Collection<?> values, String glue, boolean escapeAndQuoteStrings) {
		int valuesCount = values.size();
		if (valuesCount == 0) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		for (Object value : values) {
			if (sb.length() > 0) {
				sb.append(glue);
			}
			String string = value.toString();
			if (escapeAndQuoteStrings) {
				DatabaseUtils.appendEscapedSQLString(sb, string);
			}
			else {
				sb.append(string);
			}
		}
		return sb.toString();
	}

	/**
	 * Remove duplicate adjacent slashes and any trailing slash (unless this is the root of the file system).
	 * @param path path to fix
	 * @param withTrailingSlash whether there should be slash at the end of path 
	 * @return fixed path
	 */
	public static String fixPathSlashes(String path, boolean withTrailingSlash) {
		// Remove duplicate adjacent slashes.
		boolean lastWasSlash = false;
		char[] newPath = path.toCharArray();
		int length = newPath.length;
		int newLength = 0;
		for (int i = 0; i < length; ++i) {
			char ch = newPath[i];
			if (ch == '/') {
				if (!lastWasSlash) {
					newPath[newLength++] = File.separatorChar;
					lastWasSlash = true;
				}
			}
			else {
				newPath[newLength++] = ch;
				lastWasSlash = false;
			}
		}
		// Remove any trailing slash (unless this is the root of the file system).
		if (withTrailingSlash) {
			if (!lastWasSlash) {
				if (newLength < length) {
					newPath[newLength++] = File.separatorChar;
					return new String(newPath, 0, newLength);
				}
				else {
					char[] resultPath = Arrays.copyOfRange(newPath, 0, newLength + 1);
					resultPath[newLength++] = File.separatorChar;
					return new String(resultPath, 0, newLength);
				}
			}
		}
		else if (lastWasSlash && newLength > 1) {
			newLength--;
		}
		// Reuse the original string if possible.
		return newLength != length ? new String(newPath, 0, newLength) : path;
	}

}
