/*
 * Copyright (C) 2013 Dmytro Shkil
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.shkil.android.util.io;

import java.io.IOException;
import java.io.InputStream;

public class LimitingInputStream extends CountingInputStream {

	private final long length;

	public LimitingInputStream(InputStream in, long length) {
		super(in);
		this.length = length;
	}

	@Override
	public int read() throws IOException {
		if (getCurrentOffset() < this.length) {
			return super.read();
		}
		return -1;
	}

	@Override
	public int read(byte[] buffer, int offset, int byteCount) throws IOException {
		byteCount = (int) Math.min(byteCount, this.length - getCurrentOffset());
		if (byteCount > 0) {
			return super.read(buffer, offset, byteCount);
		}
		return -1;
	}

	@Override
	public long skip(long byteCount) throws IOException {
		byteCount = (int) Math.min(byteCount, this.length - getCurrentOffset());
		if (byteCount > 0) {
			return super.skip(byteCount);
		}
		return -1;
	}

}
