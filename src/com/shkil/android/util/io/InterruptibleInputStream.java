/*
 * Copyright (C) 2013 Dmytro Shkil
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.shkil.android.util.io;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;

public class InterruptibleInputStream extends FilterInputStream {

	private final Thread thread;

	public InterruptibleInputStream(InputStream in, Thread checkThread) {
		super(in);
		this.thread = checkThread;
	}

	@Override
	public int read() throws IOException {
		if (thread.isInterrupted()) {
			throw new InterruptedIOException();
		}
		return super.read();
	}

	@Override
	public int read(byte[] buffer, int offset, int count) throws IOException {
		if (thread.isInterrupted()) {
			throw new InterruptedIOException();
		}
		return super.read(buffer, offset, count);
	}

	@Override
	public long skip(long byteCount) throws IOException {
		if (thread.isInterrupted()) {
			throw new InterruptedIOException();
		}
		return super.skip(byteCount);
	}

}
