/*
 * Copyright (C) 2013 Dmytro Shkil
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.shkil.android.util.io;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

import android.util.TimingLogger;

public class TimingInputStream extends FilterInputStream {

	private static boolean debugTiming = true;

	private final TimingLogger timing;

	private TimingInputStream(InputStream inputStream, String tag, String label) {
		super(inputStream);
		this.timing = new TimingLogger(tag, label);
	}

	@Override
	public void close() throws IOException {
		timing.addSplit("closing");
		super.close();
		timing.dumpToLog();
	}

	public static InputStream wrap(InputStream inputStream, String tag, String label) {
		if (debugTiming) {
			return new TimingInputStream(inputStream, tag, label);
		}
		return inputStream;
	}

	public static void setDebugTiming(boolean debugTiming) {
		TimingInputStream.debugTiming = debugTiming;
	}

}
