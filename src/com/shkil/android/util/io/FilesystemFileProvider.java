/*
 * Copyright (C) 2013 Dmytro Shkil
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.shkil.android.util.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import android.os.ParcelFileDescriptor;

public class FilesystemFileProvider implements FileProvider {

	private final File directory;

	public FilesystemFileProvider(String directory) {
		this(new File(directory));
	}

	public FilesystemFileProvider(File directory) {
		this.directory = directory;
	}

	public File getDirectory() {
		return directory;
	}

	public File getFile(String fileName) throws FileNotFoundException {
		File file = new File(directory, normalizeFileName(fileName));
		if (file.exists()) {
			return file;
		}
		throw new FileNotFoundException(fileName + " not found in " + directory);
	}

	@Override
	public InputStream open(String fileName) throws IOException {
		return new FileInputStream(getFile(fileName));
	}

	@Override
	public FileDescriptorHolder openFileDescriptor(String fileName) throws IOException {
		File file = getFile(fileName);
		ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);
		return new ParcelFileDescriptorHolder(parcelFileDescriptor, 0, file.length());
	}

	@Override
	public String getAbsolutePath(String fileName) throws FileNotFoundException {
		return getFile(fileName).getPath();
	}

	@Override
	public boolean isStorageAvailable() {
		return directory.exists();
	}

	@Override
	public boolean supportsFileAccess() {
		return true;
	}

	protected String normalizeFileName(String url) {
		return url;
	}

}
