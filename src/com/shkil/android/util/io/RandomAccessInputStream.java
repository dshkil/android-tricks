/*
 * Copyright (C) 2013 Dmytro Shkil
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.shkil.android.util.io;

import java.io.IOException;
import java.io.InputStream;

public class RandomAccessInputStream extends InputStream implements RandomAccessDataProvider {

	private final RandomAccessDataProvider data;
	private long markFilePointer;

	public static RandomAccessInputStream wrap(RandomAccessDataProvider dataProvider) {
		if (dataProvider instanceof InputStream) {
			return (RandomAccessInputStream) dataProvider;
		}
		return new RandomAccessInputStream(dataProvider);
	}

	protected RandomAccessInputStream(RandomAccessDataProvider dataProvider) {
		this.data = dataProvider;
	}

	@Override
	public long length() throws IOException {
		return data.length();
	}

	@Override
	public long getFilePointer() throws IOException {
		return data.getFilePointer();
	}

	@Override
	public int read() throws IOException {
		return data.read();
	}

	@Override
	public int read(byte[] buffer, int offset, int length) throws IOException {
		return data.read(buffer, offset, length);
	}

	@Override
	public int available() throws IOException {
		return (int) (data.length() - data.getFilePointer());
	}

	@Override
	public void readFully(byte[] buffer) throws IOException {
		readFully(buffer, 0, buffer.length);
	}

	@Override
	public void readFully(byte[] buffer, int offset, int length) throws IOException {
		data.readFully(buffer, offset, length);
	}

	@Override
	public void seek(long position) throws IOException {
		data.seek(position);
	}

	@Override
	public int skipBytes(int count) throws IOException {
		return data.skipBytes(count);
	}

	@Override
	public long skip(long byteCount) throws IOException {
		if (byteCount > Integer.MAX_VALUE) {
			throw new IllegalArgumentException("byteCount must be less then Integer.MAX_VALUE");
		}
		return data.skipBytes((int) byteCount);
	}

	@Override
	public void close() throws IOException {
		data.close();
	}

	@Override
	public boolean markSupported() {
		return true;
	}

	@Override
	public void mark(int readlimit) {
		try {
			this.markFilePointer = getFilePointer();
		}
		catch (IOException ex) {
			this.markFilePointer = -1;
		}
	}

	@Override
	public synchronized void reset() throws IOException {
		if (markFilePointer == -1) {
			throw new IOException("Mark has not been set.");
		}
		seek(markFilePointer);
	}

}
