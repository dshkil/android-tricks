/*
 * Copyright (C) 2013 Dmytro Shkil
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.shkil.android.util.io;

import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;

import com.shkil.android.util.Utils;

public class AssetsFileProvider implements FileProvider {

	private final AssetManager assets;
	private final String directory;

	public AssetsFileProvider(Context context, String directory) {
		this(context.getAssets(), directory);
	}

	public AssetsFileProvider(AssetManager assets, String directory) {
		this.assets = assets;
		this.directory = Utils.fixPathSlashes(directory, true);
	}

	public String getDirectory() {
		return directory;
	}

	@Override
	public InputStream open(String fileName) throws IOException {
		return assets.open(directory + normalizeFileName(fileName));
	}

	@Override
	public FileDescriptorHolder openFileDescriptor(String fileName) throws IOException {
		AssetFileDescriptor fd = assets.openFd(directory + normalizeFileName(fileName));
		return new ParcelFileDescriptorHolder(fd.getParcelFileDescriptor(), fd.getStartOffset(), fd.getLength());
	}

	@Override
	public String getAbsolutePath(String fileName) {
		return "file:///android_asset/" + directory + normalizeFileName(fileName);
	}

	@Override
	public boolean isStorageAvailable() {
		return true;
	}

	@Override
	public boolean supportsFileAccess() {
		return true;
	}

	protected String normalizeFileName(String url) {
		return url;
	}

}
