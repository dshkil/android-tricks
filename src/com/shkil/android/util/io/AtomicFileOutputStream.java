/*
 * Copyright (C) 2013 Dmytro Shkil
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.shkil.android.util.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class AtomicFileOutputStream extends OutputStream {

	private final File fileName;
	private final File tempFileName;
	private final FileOutputStream fileOutputStream;

	public AtomicFileOutputStream(File name) throws IOException {
		this.fileName = name;
		this.tempFileName = new File(name.getAbsoluteFile() + ".tmp");
		this.fileOutputStream = new FileOutputStream(tempFileName);
	}

	@Override
	public void write(int oneByte) throws IOException {
		fileOutputStream.write(oneByte);
	}

	@Override
	public void write(byte[] buffer) throws IOException {
		fileOutputStream.write(buffer);
	}

	@Override
	public void write(byte[] buffer, int offset, int count) throws IOException {
		fileOutputStream.write(buffer, offset, count);
	}

	@Override
	public void close() throws IOException {
		fileOutputStream.getFD().sync();
		fileOutputStream.close();
		fileName.delete();
		tempFileName.renameTo(fileName);
	}

}
