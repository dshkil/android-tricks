/*
 * Copyright (C) 2013 Dmytro Shkil
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.shkil.android.util.io;

import java.io.IOException;
import java.io.OutputStream;

public class CountingOutputStream extends OutputStream {

	private final OutputStream out;
	private long bytesWritten;

	public CountingOutputStream(OutputStream out) {
		this.out = out;
	}

	public long getBytesWritten() {
		return bytesWritten;
	}

	@Override
	public void write(int oneByte) throws IOException {
		out.write(oneByte);
		bytesWritten++;
	}

	@Override
	public void write(byte[] buffer, int offset, int count) throws IOException {
		out.write(buffer, offset, count);
		bytesWritten += count;
	}

	@Override
	public void close() throws IOException {
		out.close();
	}

	@Override
	public void flush() throws IOException {
		out.flush();
	}

}
