/*
 * Copyright (C) 2013 Dmytro Shkil
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.shkil.android.util.concurrent;

import java.io.IOException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

public abstract class AsyncDrawable extends Drawable {

	private final Drawable drawable;

	public static void cancel(ImageView imageView) {
		Drawable imageViewDrawable = imageView.getDrawable();
		if (imageViewDrawable instanceof AsyncDrawable) {
			((AsyncDrawable) imageViewDrawable).cancel();
		}
	}

	public AsyncDrawable(Drawable drawable) {
		this.drawable = drawable != null ? drawable : NullDrawableHolde.NULL_DRAWABLE;
	}

	public Drawable getWrappedDrawable() {
		return drawable;
	}

	public abstract void cancel();

	@Override
	public void draw(Canvas canvas) {
		drawable.draw(canvas);
	}

	@Override
	public void setBounds(int left, int top, int right, int bottom) {
		drawable.setBounds(left, top, right, bottom);
	}

	@Override
	public void setBounds(Rect bounds) {
		drawable.setBounds(bounds);
	}

	@Override
	public boolean equals(Object o) {
		return drawable.equals(o);
	}

	@Override
	public void setChangingConfigurations(int configs) {
		drawable.setChangingConfigurations(configs);
	}

	@Override
	public int getChangingConfigurations() {
		return drawable.getChangingConfigurations();
	}

	@Override
	public void setDither(boolean dither) {
		drawable.setDither(dither);
	}

	@Override
	public void setFilterBitmap(boolean filter) {
		drawable.setFilterBitmap(filter);
	}

	@Override
	public int hashCode() {
		return drawable.hashCode();
	}

	@Override
	public Callback getCallback() {
		return drawable.getCallback();
	}

	@Override
	public void invalidateSelf() {
		drawable.invalidateSelf();
	}

	@Override
	public String toString() {
		return drawable.toString();
	}

	@Override
	public void scheduleSelf(Runnable what, long when) {
		drawable.scheduleSelf(what, when);
	}

	@Override
	public void unscheduleSelf(Runnable what) {
		drawable.unscheduleSelf(what);
	}

	@Override
	public void setAlpha(int alpha) {
		drawable.setAlpha(alpha);
	}

	@Override
	public void setColorFilter(ColorFilter cf) {
		drawable.setColorFilter(cf);
	}

	@Override
	public void setColorFilter(int color, Mode mode) {
		drawable.setColorFilter(color, mode);
	}

	@Override
	public void clearColorFilter() {
		drawable.clearColorFilter();
	}

	@Override
	public boolean isStateful() {
		return drawable.isStateful();
	}

	@Override
	public boolean setState(int[] stateSet) {
		return drawable.setState(stateSet);
	}

	@Override
	public int[] getState() {
		return drawable.getState();
	}

	@Override
	public void jumpToCurrentState() {
		drawable.jumpToCurrentState();
	}

	@Override
	public Drawable getCurrent() {
		return drawable.getCurrent();
	}

	@Override
	public boolean setVisible(boolean visible, boolean restart) {
		return drawable.setVisible(visible, restart);
	}

	@Override
	public int getOpacity() {
		return drawable.getOpacity();
	}

	@Override
	public Region getTransparentRegion() {
		return drawable.getTransparentRegion();
	}

	@Override
	public int getIntrinsicWidth() {
		return drawable.getIntrinsicWidth();
	}

	@Override
	public int getIntrinsicHeight() {
		return drawable.getIntrinsicHeight();
	}

	@Override
	public int getMinimumWidth() {
		return drawable.getMinimumWidth();
	}

	@Override
	public int getMinimumHeight() {
		return drawable.getMinimumHeight();
	}

	@Override
	public boolean getPadding(Rect padding) {
		return drawable.getPadding(padding);
	}

	@Override
	public Drawable mutate() {
		return drawable.mutate();
	}

	@Override
	public void inflate(Resources r, XmlPullParser parser, AttributeSet attrs) throws XmlPullParserException, IOException {
		drawable.inflate(r, parser, attrs);
	}

	@Override
	public ConstantState getConstantState() {
		return drawable.getConstantState();
	}

	private static class NullDrawableHolde {
		public static final Drawable NULL_DRAWABLE = new Drawable() {
			@Override
			public void draw(Canvas canvas) {}
			@Override
			public void setColorFilter(ColorFilter cf) {}
			@Override
			public void setAlpha(int alpha) {}
			@Override
			public int getOpacity() {
				return 0;
			}
		};
	}

}
