/*
 * Copyright (C) 2013 Dmytro Shkil
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.shkil.android.util.concurrent;

import java.io.InterruptedIOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public interface Fetcher<K,V> {

	public interface FetchingFuture<V> {
		boolean isDone();
		FetchingResult<V> get() throws InterruptedException, ExecutionException;
		FetchingResult<V> get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException;
		boolean cancel(boolean mayInterruptIfRunning);
		boolean isCancelled();
		FetchingFuture<V> setListener(FutureListener<V> listener);
	}

	public interface FutureListener<V> {
		void onFutureResult(FetchingResult<V> result);
	}

	public static class FetchingResult<V> {
		private V value;
		private Exception exception;

		protected FetchingResult(V value) {
			this.value = value;
		}
		protected FetchingResult(Exception exception) {
			this.exception = exception;
		}
		public V getValue() {
			return value;
		}
		public V getValueOrThrow() throws Exception {
			if (exception != null) {
				throw exception;
			}
			return value;
		}
		public Exception getException() {
			return exception;
		}
		public boolean isInterrupted() {
			return exception instanceof InterruptedIOException;
		}
		@Override
		public String toString() {
			return "FetchingResult[value=" + value + ",exception=" + exception + "]";
		}
	}

	public static class ImmediateFetchingFuture<V> implements FetchingFuture<V> {
		private final FetchingResult<V> result;

		public ImmediateFetchingFuture(V value) {
			this.result = new FetchingResult<V>(value);
		}
		public ImmediateFetchingFuture(FetchingResult<V> result) {
			this.result = result;
		}
		@Override
		public boolean isDone() {
			return true;
		}
		@Override
		public FetchingResult<V> get() {
			return result;
		}
		@Override
		public FetchingResult<V> get(long timeout, TimeUnit unit) {
			return result;
		}
		@Override
		public boolean cancel(boolean mayInterruptIfRunning) {
			return false;
		}
		@Override
		public boolean isCancelled() {
			return false;
		}
		@Override
		public FetchingFuture<V> setListener(FutureListener<V> listener) {
			listener.onFutureResult(result);
			return this;
		}
	}

	FetchingFuture<V> fetch(K key);
	FetchingFuture<V> fetch(K key, Priority priority);

}
