/*
 * Copyright (C) 2013 Dmytro Shkil
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.shkil.android.util.concurrent;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;

import com.shkil.android.util.cache.Cache;
import com.shkil.android.util.cache.LruCache;
import com.shkil.android.util.cache.SingleValueCache;

public abstract class FetcherWithCache<K,V> extends QueueFetcher<K,V> {

	protected final Cache<? super K,V> cache;

	public FetcherWithCache(Executor executor, Executor resultExecutor, int cacheSize) {
		this(executor, resultExecutor, FetcherWithCache.<K,V>newCache(cacheSize));
	}

	public FetcherWithCache(Executor executor, Executor resultExecutor, Cache<? super K,V> cache) {
		super(executor, resultExecutor);
		this.cache = cache;
		super.addListener(new FetcherListener<K,V>() {
			@Override
			public void onFetchingResult(K key, FetchingResult<V> result) {
				Cache<? super K,V> cache = FetcherWithCache.this.cache;
				V value = result.getValue();
				if (value != null) {
					synchronized (cache.getSyncLock()) {
						cache.put(key, value);
					}
				}
			}
		});
	}

	protected static <K,V> Cache<K,V> newCache(int cacheSize) {
		return cacheSize == 1 ? new SingleValueCache<K,V>() : new LruCache<K,V>(cacheSize);
	}

	@Override
	public FetchingFuture<V> fetch(K key, Priority priority) {
		V value;
		synchronized (cache.getSyncLock()) {
			value = cache.get(key);
		}
		if (value != null) {
			return new ImmediateFetchingFuture<V>(value);
		}
		return super.fetch(key, priority);
	}

	public V getValue(K key) throws InterruptedException, ExecutionException, Exception {
		V value;
		synchronized (cache.getSyncLock()) {
			value = cache.get(key);
		}
		if (value != null) {
			return value;
		}
		return fetch(key).get().getValueOrThrow();
	}

	public <T extends K> V getCachedValue(T key) {
		synchronized (cache.getSyncLock()) {
			return cache.get(key);
		}
	}

	public void clear() {
		synchronized (cache.getSyncLock()) {
			cache.clear();
		}
	}

}
