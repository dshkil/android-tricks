/*
 * Copyright (C) 2013 Dmytro Shkil
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.shkil.android.util.concurrent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.annotation.Nullable;
import javax.annotation.concurrent.GuardedBy;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.SystemClock;
import android.util.Log;

public abstract class QueueFetcher<K,V> implements Fetcher<K,V> {

	private static final String TAG = QueueFetcher.class.getSimpleName();

	public static final int RUNNING_TASKS_LIMIT = 1;

	public interface FetcherListener<K,V> {
		void onFetchingResult(K key, FetchingResult<V> result);
	}

	private final Executor resultExecutor;
	private final Object lock = new Object();
	@GuardedBy("lock")
	private final TreeMap<QueueKey,FetcherTask> tasksQueue = new TreeMap<QueueKey,FetcherTask>();
	@GuardedBy("lock")
	private final Map<K,FetcherTask> runningTasks = new HashMap<K,FetcherTask>();
	@GuardedBy("itself")
	private final List<FetcherListener<K,V>> listeners = new ArrayList<FetcherListener<K,V>>(5);

	private final Executor executor;

	private Priority defaultPriority = Priority.NORMAL;

	private class QueueKey implements Comparable<QueueKey> {
		private final K key;
		private final int hashCode;
		private long priority;

		private QueueKey(K key) {
			this.key = key;
			this.hashCode = key.hashCode();
		}
		private QueueKey(K key, long priority) {
			this.key = key;
			this.hashCode = key.hashCode();
			this.priority = priority;
		}
		public void setPriority(long priority) {
			this.priority = priority;
		}
		@Override
		@SuppressWarnings("unchecked")
		public boolean equals(Object another) {
			return another == this || ((QueueKey) another).key.equals(key);
		}
		@Override
		public int hashCode() {
			return hashCode;
		}
		@Override
		public int compareTo(QueueKey another) {
			if (another == this) {
				return 0;
			}
			int diff = (int) (another.priority - this.priority);
			if (diff != 0) {
				return diff;
			}
			if (this.equals(another)) {
				return 0;
			}
			return System.identityHashCode(this) - System.identityHashCode(another);
		}
		@Override
		public String toString() {
			return "QueueKey[key=" + key + "]";
		}
	}

	public QueueFetcher(Executor executor, @Nullable Executor resultExecutor) {
		this.executor = executor;
		this.resultExecutor = resultExecutor;
	}

	public Priority getDefaultPriority() {
		return defaultPriority;
	}

	public void setDefaultPriority(Priority defaultPriority) {
		this.defaultPriority = defaultPriority;
	}

	@Override
	public FetchingFuture<V> fetch(K key) {
		return fetch(key, defaultPriority);
	}

	@Override
	public FetchingFuture<V> fetch(K key, Priority priority) {
		FetcherTask task;
		FetchingFuture<V> future;
		boolean forceExecute = (priority == Priority.IMMEDIATE);
		synchronized (lock) {
			long priorityOrdinal = priority.toLong(SystemClock.uptimeMillis());
			task = runningTasks.get(key);
			if (task == null) {
				if (forceExecute || (tasksQueue.isEmpty() && runningTasks.size() < RUNNING_TASKS_LIMIT)) {
					task = new FetcherTask(key);
					runningTasks.put(key, task);
					forceExecute = true;
				}
				else { // enqueue task
					QueueKey queueKey = new QueueKey(key);
					task = tasksQueue.remove(queueKey);
					if (task == null) {
						task = new FetcherTask(key);
						queueKey.setPriority(priorityOrdinal);
					}
					else {
						long currentMaxPriority = findMaxPriority(task.getListeners());
						queueKey.setPriority(Math.max(priorityOrdinal, currentMaxPriority));
					}
					tasksQueue.put(queueKey, task);
				}
			}
			task.incrementUseCount();
			DeferredFetchingFuture deferredFuture = new DeferredFetchingFuture(task, priorityOrdinal);
			task.addListener(deferredFuture);
			future = deferredFuture;
		}
		if (forceExecute) {
			executor.execute(task);
		}
		return future;
	}

	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	protected void fireTaskQueueExecutor() {
		FetcherTask task = null;
		synchronized (lock) {
			if (runningTasks.size() < RUNNING_TASKS_LIMIT) {
				if (Build.VERSION.SDK_INT >= 9) {
					for (Entry<QueueKey,FetcherTask> entry = tasksQueue.pollFirstEntry(); entry != null; entry = tasksQueue.pollFirstEntry()) {
						task = entry.getValue();
						if (task.isDone()) {
							continue;
						}
						QueueKey next = entry.getKey();
						runningTasks.put(next.key, task);
						break;
					}
				}
				else {
					for (int size = tasksQueue.size(); size > 0; size--) {
						QueueKey next = tasksQueue.firstKey();
						task = tasksQueue.remove(next);
						if (task.isDone()) {
							continue;
						}
						runningTasks.put(next.key, task);
						break;
					}
				}
			}
		}
		if (task != null) {
			executor.execute(task);
		}
	}

	protected abstract V fetchValue(K key) throws Exception;

	private class FetcherTask extends FutureTask<FetchingResult<V>> {
		private final K key;
		private final List<DeferredFetchingFuture> listeners = new LinkedList<DeferredFetchingFuture>();
		private int useCount;

		public FetcherTask(final K key) {
			super(new Callable<FetchingResult<V>>() {
				@Override
				public FetchingResult<V> call() throws Exception {
					FetchingResult<V> result = null;
					try {
						result = new FetchingResult<V>(fetchValue(key));
					}
					catch (Exception ex) {
						result = new FetchingResult<V>(ex);
					}
					finally {
						synchronized (lock) {
							runningTasks.remove(key);
						}
					}
					return result;
				}
			});
			this.key = key;
		}
		@Override
		protected void done() {
			FetchingResult<V> result;
			if (isCancelled()) {
				result = new FetchingResult<V>(new CancellationException());
			}
			else {
				try {
					result = get();
				}
				catch (Exception ex) {
					result = new FetchingResult<V>(ex);
				}
			}
			fireOnReady(result);
			fireTaskQueueExecutor();
		}
		@SuppressWarnings("unchecked")
		private void fireOnReady(FetchingResult<V> result) {
			FetcherListener<K,V>[] listenersSnapshot;
			List<FetcherListener<K,V>> globalListeners = QueueFetcher.this.listeners;
			synchronized (lock) {
				synchronized (globalListeners) {
					int listenersCount = listeners.size();
					listenersSnapshot = listeners.toArray(new FetcherListener[listenersCount + globalListeners.size()]);
					for (FetcherListener<K,V> l : globalListeners) {
						listenersSnapshot[listenersCount++] = l;
					}
				}
			}
			K key = this.key;
			for (FetcherListener<K,V> l : listenersSnapshot) {
				try {
					l.onFetchingResult(key, result);
				}
				catch (RuntimeException ex) {
					Log.w(TAG, ex);
				}
			}
		}
		@GuardedBy("lock")
		protected void addListener(DeferredFetchingFuture listener) {
			listeners.add(listener);
		}
		@GuardedBy("lock")
		protected void removeListener(DeferredFetchingFuture listener) {
			listeners.remove(listener);
		}
		@GuardedBy("lock")
		protected List<DeferredFetchingFuture> getListeners() {
			return listeners;
		}
		@GuardedBy("lock")
		protected void incrementUseCount() {
			useCount++;
		}
		@Override
		public boolean cancel(boolean mayInterruptIfRunning) {
			synchronized (lock) {
				if (--useCount == 0) {
					runningTasks.remove(key);
					return super.cancel(mayInterruptIfRunning);
				}
			}
			return false;
		}
		public K getKey() {
			return key;
		}
	}

	private class DeferredFetchingFuture implements FetchingFuture<V>, FetcherListener<K,V> {
		private final AtomicBoolean cancelled = new AtomicBoolean();
		private volatile FetcherTask task;
		private volatile FetchingResult<V> result;
		private FutureListener<V> listener;
		private final long priority;

		public DeferredFetchingFuture(FetcherTask task, long priority) {
			this.task = task;
			this.priority = priority;
		}
		@Override
		public FetchingResult<V> get() throws InterruptedException, ExecutionException {
			FetcherTask task = this.task;
			if (task != null) {
				try {
					return result = task.get();
				}
				catch (InterruptedException ex) {
					this.cancel(true);
					throw ex;
				}
				catch (CancellationException ex) {
					throw new InterruptedException(ex.toString());
				}
			}
			return result;
		}
		@Override
		public FetchingResult<V> get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
			FetcherTask task = this.task;
			if (task != null) {
				try {
					return result = task.get(timeout, unit);
				}
				catch (InterruptedException ex) {
					this.cancel(true);
					throw ex;
				}
				catch (CancellationException ex) {
					throw new InterruptedException(ex.toString());
				}
			}
			return result;
		}
		@Override
		public boolean isDone() {
			FetcherTask task = this.task;
			return task == null || task.isDone();
		}
		@Override
		public boolean cancel(boolean mayInterruptIfRunning) {
			if (cancelled.getAndSet(true)) {
				return true;
			}
			FetcherTask task = this.task;
			if (task != null) {
				synchronized (lock) {
					task.removeListener(this);
					QueueKey key = new QueueKey(task.getKey());
					if (tasksQueue.remove(key) != null) {
						List<DeferredFetchingFuture> listeners = task.getListeners();
						if (listeners != null && listeners.size() > 0) {
							long maxPriority = findMaxPriority(listeners);
							key.setPriority(maxPriority);
							tasksQueue.put(key, task);
						}
					}
					return task.cancel(mayInterruptIfRunning);
				}
			}
			return false;
		}
		@Override
		public boolean isCancelled() {
			return cancelled.get();
		}
		@Override
		public void onFetchingResult(K key, FetchingResult<V> result) {
			this.result = result;
			synchronized (this) {
				if (listener != null) {
					if (resultExecutor != null) {
						resultExecutor.execute(new OnResultRunnable<V>(listener, result));
					}
					else {
						listener.onFutureResult(result);
					}
				}
			}
			this.task = null; // make eligible for gc
		}
		@Override
		public FetchingFuture<V> setListener(FutureListener<V> listener) {
			synchronized (this) {
				if (result != null) {
					if (resultExecutor != null) {
						resultExecutor.execute(new OnResultRunnable<V>(listener, result));
					}
					else {
						listener.onFutureResult(result);
					}
				}
				else {
					this.listener = listener;
				}
			}
			return this;
		}
		public long getPriority() {
			return priority;
		}
	}

	private static class OnResultRunnable<V> implements Runnable {
		private final FutureListener<V> listener;
		private final FetchingResult<V> result;

		public OnResultRunnable(FutureListener<V> listener, FetchingResult<V> result) {
			this.listener = listener;
			this.result = result;
		}
		@Override
		public void run() {
			listener.onFutureResult(result);
		}
	}

	private long findMaxPriority(List<DeferredFetchingFuture> futures) {
		if (futures == null || futures.isEmpty()) {
			return 0;
		}
		long result = 0;
		for (DeferredFetchingFuture future : futures) {
			long priority = future.getPriority();
			if (priority > result) {
				result = priority;
			}
		}
		return result;
	}

	public void addListener(FetcherListener<K,V> l) {
		synchronized (listeners) {
			listeners.add(l);
		}
	}

	public void removeListener(FetcherListener<K,V> l) {
		synchronized (listeners) {
			listeners.remove(l);
		}
	}

}
